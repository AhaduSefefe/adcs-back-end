const express = require('express');
const bcrypt = require('bcrypt');
const router = new express.Router();

// Models
const Admin = require('../models/admins');

// Utils
const { validateSignupData, validateLoginData } = require('../startup/validators');
const isAdmin = require('../middleware/isAdmin');
const auth = require('../middleware/auth');
const rateLimiter = require('../middleware/rateLimiter');

//endpoint to register admin
router.post('/',[auth, isAdmin, rateLimiter], async(req, res) => {
    
    const { valid, errors } = validateSignupData(req.body);
    if(!valid) return res.status(400).json(errors);

    let admin = new Admin(req.body);
    const salt = await bcrypt.genSalt(10);
    admin.password = await bcrypt.hash(admin.password, salt);
    await admin.save();

    const token = admin.generateAuthToken();
    res.send(token);
});

router.post('/login', rateLimiter, async(req, res) => {
    const { valid, errors } = validateLoginData(req.body);
    if (!valid) return res.status(404).json(errors);

    const admin = await Admin.findOne({username: req.body.username});
    if (!admin) return res.status(400).send('Incorrect username or password');

    const match = await bcrypt.compare(req.body.password, admin.password);
    if (!match) return res.status(400).send('Incorrect username or password');

    const token = admin.generateAuthToken();
    res.send(token);
});

//endpoint to change an admin password
router.patch('/', [auth, isAdmin, rateLimiter], async(req, res) => {
    const admin = await Admin.findById(req.user.id);
    const oldPassword = req.body.oldPassword;

    const match = await bcrypt.compare(oldPassword, admin.password);
    if(!match) return res.status(400).send("The old password is incorrect");

    const salt = await bcrypt.genSalt(10);
    const newPassword = await bcrypt.hash(req.body.newPassword, salt);

    admin.password = newPassword;
    admin.save();

    res.send("Changed Successfully!");
});

module.exports = router;